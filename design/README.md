### BREATH STYLE DESIGN

this folder contains material that can help you explore and discover the design concept of breath style.

the goal is to encourage evolving breath style further with new ideas and creativity that surpass the original concept itself.

**sample preview**

wallpaper implementation available in submission and unreleased folder

for samples, see render or svg folder for full res

2d/3d implementation example

![thumb 1](/design/thumb_1.png)

![thumb 2](/design/thumb_2.png)

![thumb 6](/design/thumb_6.png)

![thumb 5](/design/thumb_5.png)

realistic scenery implementation (see /src/unreleased/leaf)

![thumb 8](/design/thumb_8.png)

video implementation (see /src/submission_2/demo_video

![thumb 9](/design/thumb_9.png)

**paper preview**

![paper 1](/design/thumb_3.png)

![paper 2](/design/thumb_4.png)

**breath style icon concept**

same icon but slighty more rounded from breeze. possibly with more vibrant colors (needs development and consensus)

![breath icons](/design/thumb_7.png)


