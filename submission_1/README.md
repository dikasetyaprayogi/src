**SUBMISSION 1: Serenity**

![serenity wallpaper](/submission_1/thumb_1.png)

[quick download](https://gitlab.com/dikasetyaprayogi/src/-/raw/main/submission_1/serenity_light/serenity_light_desktop.png?ref_type=heads&inline=false)

a wallpaper that bring pleasant desktop experience. something that made fell at ease and refreshing whatever things need to get done. be it at work or personal activity.

**online gallery**

extra screenshots and artworks (postimg web)

1. serenity light: https://postimg.cc/gallery/cscJK8K
2. serenity dark: https://postimg.cc/gallery/R3dnWTX
3. serenity twilight: https://postimg.cc/gallery/sxRb44K

**wallpaper test**

demo video showing how the wallpaper looks in regular user activities. also showing plasma 6 centered taskmanager concept.

![](/submission_1/demo_video.mp4)

**phone mockup**

plasma mobile version

![serenity phone](/submission_1/thumb_2.png)





