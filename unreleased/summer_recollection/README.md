**UNRELEASED: Summer Recollection**

light

![summer recollection](/unreleased/summer_recollection/thumb_1.png)

dark

![summer remembrance](/unreleased/summer_recollection/thumb_2.png)

alternate

![summer memories](/unreleased/summer_recollection/thumb_3.png)

**online galleries**

summer recollection (light), summer remembrance (dark) and some alternates

https://postimg.cc/gallery/RWrV1bF


