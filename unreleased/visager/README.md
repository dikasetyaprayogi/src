**UNRELEASED: Visager**

light

![visager](/unreleased/visager/thumb_1.png)

dark

![visager](/unreleased/visager/thumb_2.png)

**online galleries**

visager (light) & voyager (dark)

https://postimg.cc/gallery/KRFVhPR

**opensuse mockup**

i think the color theme suits OpenSUSE's KDE desktop very well and i would be glad if OpenSUSE's dev want to adopt it :)

![visager](/unreleased/visager/thumb_3.png)


