**!Canceled SUBMISSION 2: Joy / replaced with Red Island**

![joy wallpaper](/unreleased/canceled_submission_2/thumb_1.png)

[quick download](https://gitlab.com/dikasetyaprayogi/src/-/raw/main/unreleased/canceled_submission_2/joy_light/joy_light_desktop.png?ref_type=heads&inline=false)

calm and vivid, a wallpaper that present joyful vibes. variation of breath style with more objects and colors in a balance.

**online gallery**

extra screenshots and artworks (postimg web)

1. joy-light: https://postimg.cc/gallery/GTzV4V6
2. joy-dark: https://postimg.cc/gallery/TyGscRG
2. joy-twilight: https://postimg.cc/gallery/rwKSPtY

**wallpaper test**

demo video showing how the wallpaper looks in regular user activities.

![](/unreleased/canceled_submission_2/demo_video.mp4)

**phone mockup**

plasma mobile version

![joy phone](/unreleased/canceled_submission_2/thumb_2.png)

