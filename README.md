## PLASMA 6 WALLPAPER & DESIGN CONCEPT

### WALLPAPER DESIGN:

i find nowadays modern wallpaper from various desktop and mobile OS tend to be simple and straightforward, less noise, more subtle and vivid. to make KDE Plasma better aligned with current trends, i want to try modernize Breeze style without losing KDE signature and uniqueness trait.

while adding simplicity, calmness and some aspect of modern wallpaper i reduced Breeze geometric triangulation into geometric string. implemented it in seamless way and therefore evolve Breeze into something more simple and clean. i named this next iteration "Breath".

KDE Plasma 4 (Air/Oxygen) -> KDE Plasma 5 (Breeze) -> KDE Plasma 6 (Breath)

to summarize: Breath is a concept of simpler, cleaner and modern evolution of Breeze. simple yet sophisticated, modern trend but still unique to KDE.

all wallpaper in this repository is set example variation of breath style all made in Inkscape.

### PLASMA 6 CONCEPT:

this is just my fun concept but i like to see a centered task manager for plasma 6. however its not a clone of other OS design because it had its own workflow and style.

![plasma 6 concept](/thumb_2.png)

[edit] after some time and reserach later im come to tealize how people will be hostile toward this kind of thing.

that doesnt bring me down though, only come with even better ideas and new desire for a complete originality.

https://discuss.kde.org/t/plasma-x-a-glimpse-of-future/7161

![plasma-x](/thumb_3.png)
