**SUBMISSION 3: Bliss**

![Bliss](/submission_3/thumb_1.png)

[download wallpaper](https://gitlab.com/dikasetyaprayogi/src/-/raw/main/submission_3/bliss_light/bliss_light_desktop.png?ref_type=heads&inline=false)

calming and soothing. helps relieves all your stress away. a breath style with modern wallpaper trend style wich emphasis in object focus ond clean background.

**online galleries**

1. bliss light: https://postimg.cc/gallery/YLR846F
2. bliss dark: https://postimg.cc/gallery/rFsLbyC
3. bliss twilight: https://postimg.cc/gallery/WD8S9RS

**wallpaper test**

demo video showing how the wallpaper looks in regular user activities.

![](/submission_3/demo_video.mp4)

**phone mockup**

plasma mobile version

![bliss phone](/submission_3/thumb_2.png)


