[!] replaced previous submission 2: joy, reason: joy is somewhat similar to serenity so i want to present a more novel design. joy will still be accessible in /src/unreleased folder.

**SUBMISSION 2: Red Island**

![red island wallpaper](/submission_2/thumb_1b.png)

[quick download](https://gitlab.com/dikasetyaprayogi/src/-/raw/main/submission_2/red_island_all_theme/red_island_desktop.png?ref_type=heads&inline=false)

the brave and calm shade of red velvet combined with KDE Plasma peaceful blue accents interface. hope that answer all the design themes suggestion in wallpaper contest announcement.

this wallpaper is a breath style implementation in "realistic scenery" similar to plasma 5.27 breeze mountain wallpaper does. a bit complex yet presented in simple manner.

**online galleries**

![red island all theme](/submission_2/thumb_3b.png)

the wallpaper itself suitable for all color theme (light/dark/twilight).

https://postimg.cc/gallery/drdy6KY

**video trailer**

the first ever example of breath style implementation in video.

![](/submission_2/video_trailer.mp4)

**phone mockup**

also great for plasma mobile version

![red island phone](/submission_2/thumb_2b.png)

**funfact**

red island or "Pulo Merah" is a tourism spot in Banyuwangi, a island located near to Bali.

occasionally, a red corals will break and become sand there making unique and beautiful red shoreline that reach its peak at dawn which is the inspiration for this wallpaper.



